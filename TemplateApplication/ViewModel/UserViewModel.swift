//
//  UserViewModel.swift
//  TemplateApplication
//
//  Created by Alexandr Chernyshev on 29/03/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

import Foundation

class UserViewModel: DynamicCollectionModel {
    var user: User
    
    init(user _user: User) {
        user = _user
        super.init(taget: user, keyPath: "friends")
    }
    
    func addFriend() {
        let position = user.friends.count;
        let newUser = User(position: position + 1)
        user.willChange(NSKeyValueChange.Insertion, valuesAtIndexes: NSIndexSet(index: position), forKey: "friends")
        user.friends.addObject(newUser);
        user.didChange(NSKeyValueChange.Insertion, valuesAtIndexes: NSIndexSet(index: position), forKey: "friends")
    }
}