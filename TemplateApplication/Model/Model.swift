//
//  Model.swift
//  TemplateApplication
//
//  Created by Alexandr Chernyshev on 29/03/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

import Foundation

var sharedModel: Model? = nil

class Model {
    
    //MARK: class methods
    class func shared() -> Model {
        if let localModel = sharedModel? {
            return localModel;
        }
        else {
            sharedModel = Model();
            return sharedModel!;
        }
    }
    
    class func dispose() {
        sharedModel = nil;
    }
    
    //MARK: lifecycle
    init() {
        
    }
}