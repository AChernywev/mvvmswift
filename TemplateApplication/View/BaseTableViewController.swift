//
//  BaseTableViewController.swift
//  TemplateApplication
//
//  Created by Alexandr Chernyshev on 29/03/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

import UIKit
import ReactiveCocoa

class BaseTableViewController: UITableViewController {
    
    dynamic var viewModel: DynamicCollectionModel! {
        didSet {
            tableView.reloadData()
        }
    }
    
    func handleChange(change: CollectionChange) {
        if(self.isViewLoaded()) {
            change .updateTableView(self.tableView);
        }
    }
    
    func rowHeightForItem(item: BaseModel) -> CGFloat {
        return 44.0
    };
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let signal = self.rac_valuesForKeyPath("viewModel.changes", observer: self).switchToLatest().subscribeNext { value in
            self.handleChange(value as CollectionChange)
        };
    }
    
    //MARK: UITableViewDataSource
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return viewModel.numberOfSections();
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let reuseIdentifier = "ReuseIdentifier"
        var localCell: UITableViewCell!
        let cell = tableView.dequeueReusableCellWithIdentifier(reuseIdentifier) as UITableViewCell?
        if let someCell = cell {
            localCell = someCell
        }
        else {
            localCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: reuseIdentifier)
        }
        
        let cellModel = viewModel.itemAtIndexPath(indexPath)
        localCell.textLabel!.text = cellModel.description
        return localCell
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        let cellModel = viewModel.itemAtIndexPath(indexPath);
        return self.rowHeightForItem(cellModel);
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        viewModel.didSelectItemAtIndexPath(indexPath, withCompletion: {
        });
    }
    
}
