//
//  ViewController.swift
//  TemplateApplication
//
//  Created by Alexandr Chernyshev on 29/03/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

import UIKit
import ReactiveCocoa

class User: BaseModel {
    private var position: Int
    dynamic var friends = NSMutableArray()
    
    override var description: String {
        get {
            return "User №\(self.position)"
        }
    }
    
    init(position _position: Int) {
        position = _position
        
    }
    
    init(friendsCount: Int) {
        position = 0
        for i in 1...friendsCount {
            let friend = User(position: i);
            friends.addObject(friend)
        }
    }
}

class ViewController: BaseTableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let modelUser = User(friendsCount: 10);
        viewModel = UserViewModel(user: modelUser)
    }
    
    @IBAction func addAction(sender: UIBarButtonItem) {
        let mdoel = self.viewModel as UserViewModel
        mdoel.addFriend()
    }
}
