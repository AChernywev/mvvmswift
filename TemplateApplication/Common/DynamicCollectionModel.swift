//
//  DynamicCollectionModel.swift
//  TemplateApplication
//
//  Created by Alexandr Chernyshev on 29/03/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

import UIKit
import ReactiveCocoa

class CollectionChange: NSObject {
    func updateTableView(tableView: UITableView) {
        tableView.reloadData()
    }
}

class CollectionChangeInsert: CollectionChange {
    private var indexPaths: NSArray!

    convenience init(indexPath: NSIndexPath) {
        self.init(indexPaths: NSArray(object: indexPath))
    }

    init(indexPaths _indexPaths: NSArray) {
        indexPaths = _indexPaths
        super.init()
    }
    
    override func updateTableView(tableView: UITableView) {
        tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: UITableViewRowAnimation.Left)
    }
}

class DynamicCollectionModel: NSObject, CollectionModel {
 
    var target: AnyObject
    var keyPath: String
    var localChanges: RACSignal!
    var items: NSMutableArray {
        get {
            let array = self.target.valueForKey(keyPath) as NSMutableArray
            return array
        }
    }
    
    var changes: RACSignal {
        get {
            return localChanges
        }
    }
    
    init(taget _target: AnyObject, keyPath _keyPath: String) {
        target = _target;
        keyPath = _keyPath
        super.init()
        self.localChanges = target.rac_valuesAndChangesForKeyPath(keyPath, options: NSKeyValueObservingOptions.allZeros, observer: self).map({ (methodTuple: AnyObject!) -> CollectionChange in
            let tuple = methodTuple as RACTuple;
            let observation = tuple.second as NSDictionary;
            let indexPaths = NSMutableArray()
            if let indexSet = observation[NSKeyValueChangeIndexesKey] as? NSIndexSet {
                indexSet.enumerateIndexesUsingBlock({ (idx, pointer) -> Void in
                    indexPaths.addObject(NSIndexPath(forItem: idx, inSection: 0))
                })
            }
            if let enumKind = observation[NSKeyValueChangeKindKey] as? UInt {
                let kind = NSKeyValueChange(rawValue: enumKind)!
                switch(kind) {
                    case .Insertion:
                        return CollectionChangeInsert(indexPaths: indexPaths)
                    default:
                        return CollectionChange()
                }
            }
            else {
                return CollectionChange()
            }
        })
    }
    
    //MARK: CollectionModel protocol
    func numberOfSections() -> Int {
        return 1;
    }
    
    func numberOfItemsInSection(section: Int) -> Int {
        return self.items.count;
    }
    
    func itemAtIndexPath(indexPath: NSIndexPath) -> BaseModel {
        let item = self.items.objectAtIndex(indexPath.row) as BaseModel;
        return item;
    }
    
    func didSelectItemAtIndexPath(indexPath: NSIndexPath, withCompletion completion: dispatch_block_t) {
        
    }
    
}