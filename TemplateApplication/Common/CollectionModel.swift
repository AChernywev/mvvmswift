//
//  CollectionModel.swift
//  TemplateApplication
//
//  Created by Alexandr Chernyshev on 29/03/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

import Foundation
import ReactiveCocoa

class BaseModel: NSObject {
    override var description: String {
        get {
            return "\(self)"
        }
    }
}

protocol CollectionModel: NSObjectProtocol {
    var changes: RACSignal {get}
    
    func numberOfSections() -> Int
    func numberOfItemsInSection(section: Int) -> Int;
    func itemAtIndexPath(indexPath: NSIndexPath) -> BaseModel;
    
    func didSelectItemAtIndexPath(indexPath: NSIndexPath, withCompletion completion: dispatch_block_t);
}