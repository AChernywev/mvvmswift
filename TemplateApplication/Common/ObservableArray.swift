//
//  ObservableArray.swift
//  TemplateApplication
//
//  Created by Alexandr Chernyshev on 29/03/15.
//  Copyright (c) 2015 Alexandr Chernyshev. All rights reserved.
//

import Foundation

class ObservableArray: NSObject {
    
    //private typealias Element = T
    dynamic private var localStorage = NSMutableArray()
    var count: Int {
        get {
            return localStorage.count;
        }
    }
    
    func addObject(object: AnyObject) {
        self.willChange(NSKeyValueChange.Insertion, valuesAtIndexes: NSIndexSet(index: self.count), forKey: "")
        localStorage.addObject(object)
        self.didChange(NSKeyValueChange.Insertion, valuesAtIndexes: NSIndexSet(index: self.count), forKey: "")
    }
    
    func objectAtIndex(index: Int) -> AnyObject {
        return localStorage[index]
    }
}
